import { getPluralCategory } from "@angular/common/src/i18n/localization";

export class Product {
    nom: string;
    fournisseur : string;
    email_fournisseur : string;
    liste_ingredients : [];
    description : string;
    age : number;
    condition_conservation : string;
    prix : number; 
    
    constructor(nom: string, fournisseur : string, email_fournisseur : string, liste_ingredients : any, description : string, age : number, condition_conservation : string, prix : number){
        this.nom = nom;
        this.fournisseur = fournisseur;
        this.email_fournisseur = email_fournisseur;
        this.liste_ingredients = liste_ingredients;
        this.description = description;
        this.age = age;
        this.condition_conservation = condition_conservation;
        this.prix = prix;
    }
   
}