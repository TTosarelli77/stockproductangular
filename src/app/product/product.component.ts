import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { array_product } from '../product_dict';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor() {}

  products_array : Array<Product> = array_product; 
  
  viewDetails(product) : string {
    return product.nom;
  }

  deleteProduct(product){
    this.products_array.splice(product);
  }

  ngOnInit() {
  }

}
